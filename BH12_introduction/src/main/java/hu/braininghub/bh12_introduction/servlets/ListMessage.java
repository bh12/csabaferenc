package hu.braininghub.bh12_introduction.servlets;

import hu.braininghub.bh12_introduction.serivice.MessageService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ListMessage extends HttpServlet {
    //private MessageService service = new MessageService();
    @Inject
    private MessageService service;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //request.setAttribute("messageList", service.getMessages());
        request.setAttribute("messageList", service.getMessageById(1));
        request.getRequestDispatcher("WEB-INF/list_message.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
