package hu.braininghub.bh12_introduction.serivice;

import hu.braininghub.bh12_introduction.model.Message;
import hu.braininghub.bh12_introduction.model.MessageDAO;
import hu.braininghub.bh12_introduction.model.MessageDTO;
import hu.braininghub.bh12_introduction.model.MessageRepository;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class MessageService {
   // MessageDAO dao = new MessageDAO();
    @Inject
    MessageDAO dao;
    
    public void addMessage(MessageDTO message) {
        MessageRepository.addMessage(message);
    }
    public void addMessage(String email, String subject, String message) {
        MessageRepository.addMessage(new MessageDTO(email, subject, message));
        try {
            dao.addNewMessage(email, subject, message);
        } catch (SQLException ex) {
            Logger.getLogger(MessageService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    public List<MessageDTO> getMessages() {
//        try {
//            return dao.getMessages();
//            //return MessageRepository.getMessages();
//        } catch (SQLException ex) {
//            Logger.getLogger(MessageService.class.getName()).log(Level.SEVERE, null, ex);
//        }
        return null;
    }
    
    public Message getMessageById(int id){
    return dao.getMessageById(id);
    }
}
