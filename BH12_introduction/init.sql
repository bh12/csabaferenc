create database bh12_messages;

use bh12_messages;

create table messages (
	email varchar(128),
    subject varchar(128),
    message text
);