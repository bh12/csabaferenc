package braininghub.servlets;

import braininghub.dao.SiteDAO;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/SiteServlet")
public class SiteServlet extends HttpServlet {

    @EJB
    SiteDAO SiteDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("site.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Integer siteID = Integer.valueOf(req.getParameter("siteId"));
        String address = req.getParameter("address");
        
        SiteDAO.addSite(siteID, address);
        
        res.sendRedirect("MainMenuServlet");
    }
}
