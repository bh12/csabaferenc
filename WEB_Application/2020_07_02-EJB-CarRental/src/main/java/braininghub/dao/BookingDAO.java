package braininghub.dao;
/* @author csaba*/

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

@Stateless
public class BookingDAO {
    
     @Resource(name = "bh12")
    DataSource ds;
    
    public void addBooking(int booking_id, LocalDate start_date, LocalDate end_date) {
        String insertQuery = "insert into booking(booking_id, start_date, end_date) values(?, ?, ?)";
        
        try (Connection conn = ds.getConnection();
                PreparedStatement st = conn.prepareStatement(insertQuery);) {
            st.setInt(1, booking_id);
            st.setDate(2, Date.valueOf(start_date));
            st.setDate(3, Date.valueOf(end_date));
            conn.close();
            
            int rowsCreated = st.executeUpdate();
            
            System.out.println(rowsCreated + " booking created.");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
