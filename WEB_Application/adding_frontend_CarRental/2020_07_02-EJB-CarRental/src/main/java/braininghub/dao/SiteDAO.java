
package braininghub.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/*@author csaba*/

@Stateless
public class SiteDAO {

@Resource(name = "bh12")
DataSource ds;

public void addSite(int siteID, String address){

    String insertQuery = "insert into site(site_id, address) values(?, ?)";

    try (Connection conn = ds.getConnection();
                PreparedStatement st = conn.prepareStatement(insertQuery);) {
            st.setInt(1, siteID);
            st.setString(2, address);
            conn.close();
            
            int rowsCreated = st.executeUpdate();
            
            System.out.println(rowsCreated + " sites created.");

        } catch (SQLException e) {
            e.printStackTrace();
        }
}
}
