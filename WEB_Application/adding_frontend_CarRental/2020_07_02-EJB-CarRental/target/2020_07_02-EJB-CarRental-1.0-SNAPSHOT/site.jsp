<%-- 
    Document   : car
    Created on : Jun 16, 2020, 5:39:21 PM
    Author     : kopacsi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/head.jsp" %>
        <title>Add Site</title>
    </head>
    <body>
         <%@include file="menu.jsp" %>
        <h1>Sites</h1>
        
        <form action="SiteServlet" method="post">
            <label for="siteId">Site ID:</label><br>
            <input type="text" id="siteId" name="siteId"><br>
            <label for="address">Address</label><br>
            <input type="text" id="address" name="address"><br><br>
            <input type="submit" value="Submit">
        </form> 
    </body>
</html>
