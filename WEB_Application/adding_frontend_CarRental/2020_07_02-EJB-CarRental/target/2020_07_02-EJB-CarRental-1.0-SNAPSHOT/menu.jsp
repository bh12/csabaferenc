<%-- 
    Document   : menu
    Created on : Jun 16, 2020, 6:57:34 PM
    Author     : kopacsi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/head.jsp" %>
        <title>Menu Page</title>
    </head>
    <body>
        <div>
        <ul>
            <li class="menulist">
                <form method="get" action="CarServlet">
                    <button class="button" type="submit">Car Servlet</button>
                </form>         
            </li>
            <li class="menulist">
                <form method="get" action="WelcomeServlet">
                    <button class="button" type="submit">Welcome Servlet</button>
                </form>
            </li>
            <li class="menulist">
                <form method="get" action="BookingServlet">
                    <button class="button" type="submit">Booking Servlet</button>
                </form>         
            </li>
        </ul>
        </div>
    </body>
</html>
