<%-- 
    Document   : car
    Created on : Jun 16, 2020, 5:39:21 PM
    Author     : kopacsi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/head.jsp" %>
        <title>Add Car</title>
    </head>
    <body>
         <%@include file="menu.jsp" %>
        <h1>Add new car</h1>
        
        <form action="CarServlet" method="post">
            <label for="carId">Car ID:</label><br>
            <input type="text" id="carId" name="carId"><br>
            <label for="details">Details</label><br>
            <input type="text" id="details" name="details"><br><br>
            <input type="submit" value="Submit">
        </form> 
    </body>
</html>
