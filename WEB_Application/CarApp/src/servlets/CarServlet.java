package servlets;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CarServlet")
public class CarServlet extends HttpServlet {

    private static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/car_data?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String INSERT_NEW_CAR = "INSERT INTO car (site_id, plate_number, color, engine_type, weight, year_of_built) VALUES (?, ?, ?, ?, ?, ?)";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        res.setContentType("text/html");
        req.getRequestDispatcher("AddCar.jsp").forward(req,res);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // adatbázis frissítése az űrlapról érkező adatokkal
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL, "root", "admin");
             PreparedStatement ps = conn.prepareStatement(INSERT_NEW_CAR);) {

            int siteID = Integer.parseInt(req.getParameter("SiteId"));
            String plateNumber = req.getParameter("PlateNumber");
            String color = req.getParameter("Color");
            String engineType = req.getParameter("EngineType");
            int weight = Integer.parseInt(req.getParameter("Weight"));
            LocalDate yearOfBuilt = LocalDate.parse(req.getParameter("YearOfBuilt"));

//color, engine_type, weight, year_of_built
            ps.setInt(1,siteID);
            ps.setString(2,plateNumber);
            ps.setString(3,color);
            ps.setString(4,engineType);
            ps.setInt(5,weight);
            ps.setDate(6, Date.valueOf(yearOfBuilt));

            ps.executeUpdate();
            resp.sendRedirect("MainMenuServlet");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
