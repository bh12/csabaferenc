package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

@WebServlet("/SiteServlet")

public class SiteServlet extends HttpServlet {

    private static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/car_data?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String INSERT_NEW_SITE = "INSERT INTO site (address) VALUES (?)";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        req.getRequestDispatcher("Site.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL, "root", "admin");
             PreparedStatement ps = conn.prepareStatement(INSERT_NEW_SITE);) {

            String address = req.getParameter("address");

            ps.setString(1,address);

            ps.executeUpdate();
            resp.sendRedirect("MainMenuServlet");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
