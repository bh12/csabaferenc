<%--
  Created by IntelliJ IDEA.
  User: csaba
  Date: 2020. 06. 16.
  Time: 20:09
  To change this template use File | Settings | File Templates.
  -----------------------------------------------------------------
  Hozzunk létre az adatbázisunkban egy Car táblát
  (CarId, PlateNumber, Color, EngineType, Weight, YearOfBuilt, SiteId).
A servlets.CarServlet-en keresztül a felhasználó vehessen fel egy új autót az adatbázisba.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add new car</title>
</head>
<body>
<form action="CarServlet" method="post">

    <label for="SiteId">Site Id:</label><br>
    <input type="number" id="SiteId" name="SiteId"><br>

    <label for="PlateNumber">Plate Number:</label><br>
    <input type="text" id="PlateNumber" name="PlateNumber"><br>

    <label for="Color">Color:</label><br>
    <input type="text" id="Color" name="Color"><br>

    <label for="EngineType">Engine Type:</label><br>
    <input type="text" id="EngineType" name="EngineType"><br>

    <label for="Weight">Weight:</label><br>
    <input type="number" id="Weight" name="Weight"><br>

    <label for="YearOfBuilt">Year Of Built:</label><br>
    <input type="date" id="YearOfBuilt" name="YearOfBuilt"><br><br>

    <input type="submit" value="Create car">
</form>
</body>
</html>
