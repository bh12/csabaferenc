/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bh12library.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

import static com.mycompany.bh12library.entity.BookEntity.QUERY_FIND_ALL;

/**
 *
 * @author kopacsi
 */
@Entity
@Table(name = "book")
@NamedQuery(name = QUERY_FIND_ALL, query = "select b from BookEntity b")
public class BookEntity extends BaseEntity {

    public static final String QUERY_FIND_ALL = "BookEntity.findAll";
    
    @Column(unique = true)
    private String isbn;
    
    @Column
    private String title;
    
    @Column
    private String description;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
    name = "book_author", 
    joinColumns = @JoinColumn(name = "book_id"), 
    inverseJoinColumns = @JoinColumn(name = "author_id"))
    private List<AuthorEntity> authors = new ArrayList<>();

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AuthorEntity> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorEntity> authors) {
        this.authors = authors;
    }
    
    public void addAuthor(AuthorEntity entity) {
        authors.add(entity);
    }
    
}
