package com.mycompany.bh12library.dao;

import com.mycompany.bh12library.dto.BookFilterDTO;
import com.mycompany.bh12library.entity.BookEntity;

import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Collections;
import java.util.List;

@Singleton
@TransactionAttribute
public class BookDAO
{
    private static final String QUERY_FIND_BY_FILTER = "select b from BookEntity b ";

    @PersistenceContext
    private EntityManager em;

    public List<BookEntity> findAll()
    {
        TypedQuery<BookEntity> namedQuery = em.createNamedQuery(BookEntity.QUERY_FIND_ALL, BookEntity.class);
        return namedQuery.getResultList();
    }

    public List<BookEntity> findByFilter(BookFilterDTO filter)
    {
        if (filter.getAuthorName() == null && filter.getDescription() == null && filter.getIsbn() == null &&
                filter.getTitle() == null)
        {
            return findAll();
        }
        String queryString = QUERY_FIND_BY_FILTER;
        if (!filter.getAuthorName().isEmpty())
        {
            queryString += "join b.authors a where a.name like concat ('%', :name, '%')";
        }

        if (!filter.getTitle().isEmpty())
        {
            if (queryString.contains("join"))
            {
                queryString += " and b.title like concat ('%', :title, '%')";
            }
            else
            {
                queryString += " where b.title like concat ('%', :title, '%')";
            }
        }

        if (!filter.getIsbn().isEmpty()) {
            if (queryString.contains("where")) {
                queryString += " and b.isbn like concat ('%', :isbn, '%')";
            } else {
                queryString += " where b.isbn like concat ('%', :isbn, '%')";
            }
        }

        if (!filter.getDescription().isEmpty()) {
            if (queryString.contains("where")) {
                queryString += " and b.description like concat ('%', :description, '%')";
            } else {
                queryString += " where b.description like concat ('%', :description, '%')";
            }
        }

        TypedQuery<BookEntity> query = em.createQuery(queryString, BookEntity.class);
        if (queryString.contains(":name")) {
            query = query.setParameter("name", filter.getAuthorName());
        }
        if (queryString.contains(":title")) {
            query = query.setParameter("title", filter.getTitle());
        }
        if (queryString.contains(":isbn")) {
            query = query.setParameter("isbn", filter.getIsbn());
        }
        if (queryString.contains(":description")) {
            query = query.setParameter("description", filter.getDescription());
        }


        List<BookEntity> resultList = query.getResultList();
        return resultList;
    }

    public void save(BookEntity bookEntity)
    {
        em.persist(bookEntity);
    }
}
