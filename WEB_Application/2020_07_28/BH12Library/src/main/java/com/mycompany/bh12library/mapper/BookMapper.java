package com.mycompany.bh12library.mapper;

import com.mycompany.bh12library.dto.BookDTO;
import com.mycompany.bh12library.entity.BookEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BookMapper
{
    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

    BookDTO toDTO(BookEntity bookEntity);

    List<BookDTO> toDTOList(List<BookEntity> bookEntities);
}
