/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bh12library.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author kopacsi
 */
@Entity
@Table(name = "author" )
public class AuthorEntity extends BaseEntity {
    
    @Column
    private String name;
    
    @ManyToMany(mappedBy = "authors")
    private List<BookEntity> books;
}
