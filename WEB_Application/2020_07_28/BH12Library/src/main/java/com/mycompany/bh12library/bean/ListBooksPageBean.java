package com.mycompany.bh12library.bean;

import com.mycompany.bh12library.dto.BookDTO;

import java.util.List;

public class ListBooksPageBean
{
    List<BookDTO> books;

    public List<BookDTO> getBooks()
    {
        return books;
    }

    public void setBooks(List<BookDTO> books)
    {
        this.books = books;
    }
}
