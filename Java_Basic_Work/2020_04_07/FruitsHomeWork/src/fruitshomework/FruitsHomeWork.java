/*
 Hazi feladat: Legyen egy Fruit osztalyod. Minden Fruitnak van valamilyen szine es kilogrammonkenti ara. 
Ebbol szarmaztass le egy Banana, Apple, Orange osztalyt. A Banana osztaly specialis, 
mert annak mindig a dekagrammonkenti arat szeretnenk megtudni. 
Az Applenek van iz tipusa is. Pl lehet fanyar, edes, stb.. egy Apple. 
Az Orange mindig naranccsarga szinu.

 */
package fruitshomework;

/**
 *
 * @author csaba
 */
public class FruitsHomeWork {

  
    public static void main(String[] args) {
        
        Banana banana = new Banana("yellow", 500);
        banana.setPrice(500);
        System.out.println(banana);
        
        Apple apple = new Apple("red", 400);
        apple.setTaste("sweet");
        System.out.println(apple);
        
        Orange orange = new Orange(280);
        orange.setColor("");
        System.out.println(orange);
    }
    
}
