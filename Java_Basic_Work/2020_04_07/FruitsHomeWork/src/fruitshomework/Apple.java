/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fruitshomework;

/**
 *
 * @author csaba
 */
public class Apple  extends Fruit{
    private String taste;

    public Apple(String color, double price) {
        super(color, price);
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    @Override
    public String toString() {
        return "Apple " + "color = " + color + ", price = " + price + " taste = " + taste;
    }

    
}
