package fruitshomework;

/**
 *
 * @author csaba
 */
public class Fruit {

    protected String color;
    protected double price;

    public Fruit(String color, double price) {
        this.color = color;
        this.price = price;
    }

    public Fruit(double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Fruit: " + "color = " + color + ", price = " + price;
    }

}
