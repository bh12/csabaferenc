/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fruitshomework;

/**
 *
 * @author csaba
 */
public class Banana extends Fruit{

    private final int KG_TO_DKG = 100;
    
    public Banana(String color, double price) {
        super(color, price);
    }

    @Override
    public void setPrice(double price) {
        super.setPrice(price / KG_TO_DKG); 
    }

    @Override
    public double getPrice() {
        return super.getPrice() / KG_TO_DKG; 
    }

    @Override
    public String toString() {
        return "Banana: " + "color = " + color + ", price = " + price;
    }
    
}
