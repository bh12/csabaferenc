/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fruitshomework;

/**
 *
 * @author csaba
 */
public class Orange extends Fruit{

    private final String ORANGE_COLOR = "orange";

    public Orange(double price) {
        super(price);
    }

    @Override
    public void setColor(String color) {
        super.setColor(ORANGE_COLOR); 
    }

    @Override
    public String getColor() {
        return ORANGE_COLOR; 
    }

    @Override
    public String toString() {
        return "Orange: " + "color = " + color + ", price = " + price;
    }
    
    
    
}
