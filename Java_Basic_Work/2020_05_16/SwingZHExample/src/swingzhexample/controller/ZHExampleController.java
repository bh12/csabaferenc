/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingzhexample.controller;

import com.sun.jmx.snmp.Timestamp;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import swingzhexample.model.ZHExampleModel;
import swingzhexample.view.ZHExampleView;

/**
 *
 * @author csaba
 */
public class ZHExampleController {

    private ZHExampleModel model;
    private ZHExampleView view;
    private int counter = 0;

    public ZHExampleController() {
        this.model = new ZHExampleModel();
        this.view = new ZHExampleView(this);
        this.view.init();
    }

    public void handleButtonPress() {

        model.setEventLog(++counter + ". " + currentTime() + " : Button Pressed");
        view.setFieldText(model.getEventLog());
        WriteToFile();
    }

    public String currentTime() {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String date = dtf.format(now);
        return date;
    }

    public void WriteToFile() {
        
        try (FileWriter fw = new FileWriter(model.f,true);
                BufferedWriter bw = new BufferedWriter(fw)) {
            bw.append(model.getEventLog());
            bw.newLine();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
