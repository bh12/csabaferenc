/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingzhexample.view;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import swingzhexample.controller.ZHExampleController;

/**
 *
 * @author csaba
 */
public class ZHExampleView extends JFrame {

    private ZHExampleController controller;

    private JTextArea textArea;
    private JButton button;
    private JPanel panel;

    public ZHExampleView(ZHExampleController controller) {
        this.controller = controller;

    }

    public void init() {
        setUpWindow();
        setUpTextField();
        setUpButton();
        showWindow();
    }

    private void setUpWindow() {
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("SwingZhExample");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setLayout(new GridBagLayout());

        add(panel);
    }

    private void showWindow() {
        this.setVisible(true);
    }

    private void setUpTextField() {
        this.textArea = new JTextArea("...");
        Font f = new Font("Arial", Font.BOLD, 20);
        this.textArea.setFont(f);
        // this.textArea.set
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 5;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(this.textArea, gbc);

    }

    private void setUpButton() {
        this.button = new JButton("PrintCurrentTime");

        this.button.addActionListener(event -> {
            controller.handleButtonPress();
        });
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 2;
        gbc.gridx = 4;
        gbc.gridy = 3;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(this.button, gbc);
    }

    public void setFieldText(String screenText) {
        this.textArea.setText(screenText);
    }

}
