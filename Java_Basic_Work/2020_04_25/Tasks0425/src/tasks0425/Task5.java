/*
 A program kérjen be egy elérési és egy cél útvonalat. 
 Az elérési útvonalon lévő fájlt tartalmával együtt másolja át a cél útvonalra. 
 Ha a megadott fájl egy mappa, akkor dobjunk kivételt amit le is kezelünk. 
 Ha bármi hiba történik (például a cél útvonal ugyanaz mint az elérési útvonal) akkor is dobjunk kivételt, és kezeljük le.
 */
package tasks0425;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author csaba
 */
public class Task5 {

    protected void copyFile() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Forrás elérési útvonala: ");
        String source = sc.nextLine();

        System.out.println("Cél útvonal: ");
        String target = sc.nextLine();

        File sourceFile = new File(source);
        File targetFile = new File(target);

        try (BufferedReader br = new BufferedReader(new FileReader(sourceFile));
                FileWriter fw = new FileWriter(targetFile)) {
            String line = br.readLine();
            while(line != null){
                System.out.println(line);
                line = br.readLine();
                fw.write(line);
            }
        } catch (IOException e) {
            System.out.println("Fájl elérési hiba");
        }
    }
}
