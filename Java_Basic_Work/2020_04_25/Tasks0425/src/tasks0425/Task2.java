/*
 Írjuk ki a felhasználó által megadott útvonalban lévő összes fájl illetve mappa nevet.
 */
package tasks0425;

import java.io.File;
import java.util.Scanner;

/**
 *
 * @author csaba
 */
public class Task2 {

    protected void printFileNames() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Kérem az elérési utat: ");

        String filePath = sc.next();

        File f = new File(filePath);

        for (String s : f.list()) {
            System.out.println(s);
        }
    }

}
