/*
Írjuk ki egy fájl tulajdonságait 
(fájl-e?, útvonal, abszolút útvonal, szülőmappa, fájl neve, mérete, olvasható-e, írható-e, rejtett-e, utolsó módosítás dátuma)
 */
package tasks0425;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

/**
 *
 * @author csaba
 */
public class Task1 {

    protected void fileProperities(){
    
       File f = new File ("teszt.txt");
        try(FileWriter fw = new FileWriter(f)){
            
           fw.append("Ez egy teszt szöveg.");
        
        System.out.println("Fájl-e : " + f.isFile());
        System.out.println("Útvonal: " + f.getPath());
        System.out.println("Abszolút útvonal: " + f.getAbsolutePath());
        System.out.println("Szülőmappa: " + f.getParent());
        System.out.println("Fájl neve: " + f.getName());
        System.out.println("Fájl mérete: " + f.length());
        System.out.println("Olvasható-e: " + f.canRead());
        System.out.println("Írható-e: " + f.canWrite());
        System.out.println("Rejtett-e: " + f.isHidden());
        System.out.println("Utolsó módosítás dátuma: " + f.lastModified()); 
        
        }
        catch(IOException e){
            
            System.out.println("Fájlkezelési hiba");
            }
    }
 
    
   
}
