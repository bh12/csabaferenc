/*
 A program olvassa be a „scanner_text.txt” fájl tartalmát és írja ki a konzolra.
 */
package tasks0425;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author csaba
 */
public class Task4 {
    protected void printFromFile(){
    
        File f = new File("scanner_text.txt");
        
        try(FileReader fr = new FileReader(f);
                BufferedReader br = new BufferedReader(fr)){
        String line;
            do{
            line = br.readLine();
            if (line.length() != 0){   
            System.out.println(line);
            }
            
            }while(!line.equals("\n") && line.length()!=0);//line.length helyett line
        } catch (IOException e) {
            System.out.println("Fájl elérési hiba.");
        } catch (NullPointerException n){
            System.out.println(" [NULLPOINTER EXCEPTION] - tetten érve ");
        }
    }
}
 