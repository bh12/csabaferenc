/*
 Készítsen lottó szelvény készítő programot. A program a felhasználó által 
 megadott darabszámú lottó szelvényt generáljon, és mindet egy külön fájlba írja ki 
 olyan formátumban, hogy majd ezt később be is tudjuk olvasni.
---------------------------------------------------------------------------------------
task7: Olvassuk be az előző feladatban generált összes lottószelvényt, és írjuk ki őket soronként a konzolra.
----------------------------------------------------------------------------------------------------------------
task8:

Az előző feladatokat módosítsuk úgy, hogy a program minden egyes futásnál 
az épp aktuális idő szerinti mappába teszi a szelvényeket, illetve az összes ilyen mappát olvassuk fel és írjuk ki a konzolra.
 */
package tasks0425;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author csaba
 */
public class Task6 {

    public static void main(String[] args) {
        lottoCouponCreator();
    }

    public static void lottoCouponCreator() {

        Scanner sc = new Scanner(System.in);
        int counter;

        String path = "LotteryCoupons.txt";
        File f = new File(path);

        do {
            System.out.println("Hány szelvényt generáljunk? ");
            counter = sc.nextInt();
            if (counter < 1) {
                System.out.println("Nem lehet 1-nél kevesebb!");
            }
        } while (counter <= 0);

        try (FileWriter fw = new FileWriter(f);
                BufferedWriter bw = new BufferedWriter(fw)) {

            for (int i = 0; i < counter; i++) {

                int[] lotto = lottoGenerator();

                for (int numbers : lotto) {
                    bw.write(numbers + " ");            //stringbuilderrel
                }
                bw.newLine();
            }
        } catch (IOException ex) {
            System.out.println("File hiba");
        }
    }

    public static int[] lottoGenerator() {

        Random random = new Random();

        int generatedNumber;
        int[] lottoNumbers = new int[5];
        boolean newNumber;

        for (int i = 0; i < lottoNumbers.length; i++) {

            do {

                generatedNumber = random.nextInt(89) + 1;
                lottoNumbers[i] = generatedNumber;
                newNumber = true;

                for (int j = 0; j < i; j++) {

                    if (generatedNumber == lottoNumbers[j]) {
                        newNumber = false;
                    }
                }

            } while (newNumber == false);
        }

//        for (int lottoNumber : lottoNumbers) {
//            System.out.print(lottoNumber + ", ");
//        }
        return lottoNumbers;
    }

}
