/*
 Kérjünk be a felhasználótól szöveges sorokat addig, amíg egy üres sort nem ír be.
A sorokat sorszámmal írjuk ki egy fájlba „scanner_text.txt” néven.
 */
package tasks0425;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author csaba
 */
public class Task3 {
    
    protected void printLinesToFile(){
    
        Scanner sc = new Scanner(System.in);
        
        int lineCounter = 1;
        
        File f = new File("scanner_text.txt");
        
        System.out.println("Sorok beolvasása üres sorig: ");
        
        try(FileWriter fr = new FileWriter(f)){
            String line;
        do{
        line = sc.nextLine();
        fr.write(lineCounter + " ." + line + "\n");
        lineCounter++;
        }while(!line.equals(""));
        } catch (IOException e) {
            System.out.println("Fájl hiba");
        }
        
    }
}
