/*
 Készíts egy banki utaló rendszert, ami szimulál személyek közötti átutalásokat.
 Minden személynek legyen neve, bankszámlaszáma, és egyenlege. 
 Egy utalásnak a következőket kell tartalmaznia: utaló személy, cél személy, átutalandó összeg, teljesítés dátuma.
 Ha az utalást indítványozó személnyek nincs megfelelő egyenlege,
 akkor ezt kezeljük le egy saját kivételosztállyal (pl InsufficientFundsException).
 A program véletlenszerűen generáljon le személyeket, és ezen személyek közötti utalásokat.
 Írjuk ki a konzolra:
 a.    a személyek neveit és kezdő egyenlegét
 b.    az utalások adatait, dátum alapján növekvő sorrendben
 c.    a személyek neveit és végső egyenlegét, egyenleg változásukat
 d.    hibás utalásokat és miért voltak hibásak

 Az előző rendszerünket módosítsuk úgy, hogy fájl alapú adatbetöltéssel illetve kiírással működjön. 
 Több bank utalásait is tudja szimulálni, és ezeket rendszerezett módon írjuk ki.
 */
package banksimulation;

import banksimulation.dataload.DataLoader;
import banksimulation.dataload.FileDataLoader;
import banksimulation.dataload.JavaDataLoader;
import banksimulation.dataprint.ConsoleDataPrinter;
import banksimulation.dataprint.DataPrinter;
import banksimulation.exceptions.IllegalTargetClientException;
import banksimulation.exceptions.InsufficientFundsException;
import banksimulation.exceptions.BankingException;
import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.util.ArrayList;
import java.util.List;

public class BankApplication {

    public static final int CLIENT_BASE_BALANCE = 200000;

    public static final int MAX_TRANSFER_AMMOUNT = 200000;

    public static boolean isAfterSimulate = false;

    public static List<Client> clients = new ArrayList<>();
    public static List<Transfer> transfers = new ArrayList<>();
    public static List<BankingException> bankingExceptions = new ArrayList<>();

    public static void main(String[] args) {

        DataLoader loader = new JavaDataLoader();
        loader.loadInitialData();

        DataPrinter printer = new ConsoleDataPrinter();
        printer.printData();

        simulate(transfers, bankingExceptions);

        calculateBalanceModification(clients);
        
        printer.printData();

    }

    public static void simulate(List<Transfer> transfers, List<BankingException> bankingExceptions) {
        for (Transfer t : transfers) {
            try {
                simulateTransfer(t);
            } catch (BankingException e) {
                bankingExceptions.add(e);
            }
        }
        isAfterSimulate = true;
    }

    public static void simulateTransfer(Transfer t) throws InsufficientFundsException, IllegalTargetClientException {
        Client source = t.getSource();
        Client target = t.getTarget();

        long ammount = t.getAmmount();

        long sourceBalance = source.getBalance();
        long targetBalance = target.getBalance();
        if (sourceBalance >= ammount) {
            source.setBalance(sourceBalance - ammount);
            target.setBalance(targetBalance + ammount);
        } else {
            throw new InsufficientFundsException(t);
        }

        if (source.equals(target)) {
            throw new IllegalTargetClientException(t);
        }
    }

    public static void calculateBalanceModification(List<Client> clients) { 
        
        for (Client c : clients) {
            c.setAmmountModification(c.getBalance() - c.getStarterBalance());
        }
    }

}
