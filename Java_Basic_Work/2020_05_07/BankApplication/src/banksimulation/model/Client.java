package banksimulation.model;

public class Client {

    private String clientId;

    private String accountNumber;

    private long balance;

    private long starterBalance;
    
    private long ammountModification;

    public Client(String clientId, String accountNumber, long balance) {
        this.clientId = clientId;
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public long getAmmountModification() {
        return ammountModification;
    }

    public void setAmmountModification(long ammountModification) {
        this.ammountModification = ammountModification;
    }

    public long getStarterBalance() {
        return starterBalance;
    }

    public void setStarterBalance(long starterBalance) {
        this.starterBalance = starterBalance;
    }

    @Override
    public String toString() {
        return "Client{" + "clientId=" + clientId + ", accountNumber=" + accountNumber + ", balance=" + balance + '}';
    }

}
