package banksimulation.dataload;

import banksimulation.BankApplication;
import static banksimulation.BankApplication.clients;
import static banksimulation.BankApplication.transfers;
import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class FileDataLoader implements DataLoader {

    @Override
    public void loadInitialData() {
        clientDataLoader();
        transferDataLoader();
    }

    public void clientDataLoader() throws NumberFormatException {
        File clientFile = new File("clients.txt");

        try (FileReader fr = new FileReader(clientFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] clientArray = line.split(",");

                String clientId = clientArray[0];
                String accountNumber = clientArray[1];
                long balance = Long.parseLong(clientArray[2]);

                Client c = new Client(clientId, accountNumber, balance);

                BankApplication.clients.add(c);

                line = br.readLine();
            }
        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben! :(");
        }
    }

    public void transferDataLoader() {

        File clientFile = new File("transfers.txt");

        List<String> clientList = BankApplication.clients.stream()
                .map(client -> client.getClientId())
                .collect(Collectors.toList());

        try (FileReader fr = new FileReader(clientFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] transferArray = line.split(",");

                String clientSource = transferArray[0];
                String clientTarget = transferArray[1];
                long ammount = Long.parseLong(transferArray[2]);
// clients listából kell kivenni a source/target id-t
                int sourceIndex = clientList.indexOf(clientSource);
                int targetIndex = clientList.indexOf(clientTarget);

                Client cSource = clients.get(sourceIndex);
                Client cTarget = clients.get(targetIndex);

                Transfer t = new Transfer(cSource, cTarget, ammount);
                BankApplication.transfers.add(t);

                line = br.readLine();
            }

        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben! :(");
        }

    }

}
