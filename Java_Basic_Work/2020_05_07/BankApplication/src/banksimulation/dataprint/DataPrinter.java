
package banksimulation.dataprint;

/**
 *
 * @author csaba
 */
public interface DataPrinter {
    
    public void printData();
}
