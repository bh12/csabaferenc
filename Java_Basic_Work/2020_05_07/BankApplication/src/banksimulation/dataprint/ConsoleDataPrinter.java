package banksimulation.dataprint;

import static banksimulation.BankApplication.bankingExceptions;
import static banksimulation.BankApplication.clients;
import static banksimulation.BankApplication.isAfterSimulate;
import static banksimulation.BankApplication.transfers;
import banksimulation.exceptions.BankingException;
import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.util.List;

/**
 *
 * @author csaba
 */
public class ConsoleDataPrinter implements DataPrinter {
    
    @Override
    public void printData() {

        if (!isAfterSimulate) {
            printClientDetails(clients, "kezdő");
            printTransfers(transfers);
        } else {
            printClientDetails(clients, "végső");
            printErrors(bankingExceptions);
            
            
        }
    }

    public static void printTransfers(List<Transfer> transfers) {
        System.out.println("Utalások adatai:");
        for (Transfer t : transfers) {
            System.out.print("Küldő:" + t.getSource().getClientId());
            System.out.print("\t | Kedvezményezett:" + t.getTarget().getClientId());
            System.out.print("\t | Összeg:" + t.getAmmount());
            System.out.print("\t | Teljesítés dátuma:" + t.getDateOfCompletion());
            System.out.println("");
        }
    }

    public static void printErrors(List<BankingException> bankingExceptions) {
        System.out.println("Hiba lista:");
        for (BankingException b : bankingExceptions) {
            System.out.println(b.getMessage() + " |");
            System.out.print(b.getTransfer().toString());
            System.out.println("");
        }
    }

    public static void printClientDetails(List<Client> clients, String balanceStatus) {
        System.out.println("Kliensek azonosítói és " + balanceStatus + " egyenlegei:");
        for (Client c : clients) {
            System.out.print("Azonosító:" + c.getClientId());
            System.out.print("\t | " + balanceStatus + " egyenleg:" + c.getBalance());
            if (isAfterSimulate){
                System.out.println("\t | " + "Egyenleg változás: " + c.getAmmountModification());
            }
            System.out.println("");
        }
    }
    
}
