/*
 Olvassuk be az „animals.csv” fájlt. Ebben található állatokról készítsünk statisztikát:
 a.    Írjuk ki hány állatot tartalmaz a fájl, illetve hány tulajdonságot (a nevet ne számoljuk bele)
 b.    Írjuk ki hány állatnak van szőre, hánynak nincs
 c.    Írjuk ki hány állatnak van tolla, hánynak nincs
 d.    Írjuk ki az adott lábszámhoz tartozó állatok darabszámát
 e.    Írjuk ki a class_type alapján a típusokhoz tartozó állatok darabszámát
 f.    Írjuk ki az összes nem lélegző állat nevét
 g.    Írjuk ki azon állatok neveit és az összes tulajdonságait, amik gerinces és ragadózó állatok.
 */
package animals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author csaba
 */
public class Animals {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        File f = new File("animals.csv");
        int lineCounter = 0;
        int columnCounter = 0;
        StringBuilder sb = new StringBuilder();
        String[][] array = new String[110][20];
        String animals = "";
        try (FileReader fr = new FileReader(f);
                BufferedReader br = new BufferedReader(fr)) {

            // System.out.println(animals);
            while (animals != null) {

                sb.append(animals);
              
                animals = br.readLine();
                
               // columnCounter = animals.split(";").length;

//            for (int i = 0; i < array.length; i++) {
//                for (int j = 0; j < array[i].length; j++) {
//                    
//                     array[i][j] = tempArray[j];
//                     System.out.println(array[i][j] + " ");
//                }
//            }
               // append -> ; -> split, tömb, keresés index alapján: ha tömb[i] contains "hair" index visszaad -> index alapján kiszámolni hányadik elem az adott tulajdonság
                sb.append("\n");

                if (animals != null) {
                    lineCounter++;
                    System.out.println(lineCounter + ". " + animals);
                }
                // splitelni -> tömbbe tárolni -> feldolgozni
            }

            //  System.out.println(sb);
           
        } catch (IOException e) {
            System.out.println("File hiba");
        }

         System.out.println("Állatok száma: " + (lineCounter - 1));
         System.out.println(" állatnak van szőre, hánynak nincs");
         
        
        String [] sorok = sb.toString().split("\n");
        System.out.println("-------------------------------------------------");
        
        String [] szel = sorok[1].split(";");
        columnCounter = szel.length;
        
        for (int i = 0; i < szel.length; i++) {
            if (szel[i].matches("hair")){
                System.out.println("haj: " + i);
            }
        }
        
        
        System.out.println(szel.length);
        System.out.println(sorok[1]);
        System.out.println(szel[0]);
        
//        for (int i = 0; i < sorok.length; i++) {
//            System.out.println(sorok[i]);
//            
//        }
      
    }

}
