/*
Hozzunk létre egy Person osztályt is, amiben minden személynél eltárolju, hogy melyik pénznemből hány darab van neki.
Tároljuk el még a személy korát és nevét is.
*/
package money;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author csaba
 */
public class Person {
    
    private String name;
    private int age;
    
    Map<Currency, Integer> moneyMap = new HashMap<>();

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Map<Currency, Integer> getMoneyMap() {
        return moneyMap;
    }

    public void setMoneyMap(Currency type,int amount) {
        this.moneyMap.put(type, amount);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.moneyMap);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (!Objects.equals(this.moneyMap, other.moneyMap)) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "Person {" + " name = " + name + ", age = " + age + '}';
    }
    
}
