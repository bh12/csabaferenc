/*
Hozz létre egy Currency enumot. Tároljuk el benne a pénznemet (EUR, GBP, HUF, USD, JPN, RMB) és a forintbeli értékét integerként. 
*/
package money;

/**
 *
 * @author csaba
 */
public enum Currency {
    EUR(350), 
    GBP(410),
    HUF(1),
    USD(340),
    JPN(270),
    RMB(265);
    
    private final int value;

    private Currency(int value) {
        this.value = value;
    }
     public int getInHuf() {
        return value;
    }
     
     public int fortune(Currency c,int amount){
     return c.value * amount;
     }
}
