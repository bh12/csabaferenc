/*
 Hozz létre egy Currency enumot. Tároljuk el benne a pénznemet (EUR, GBP, HUF, USD, JPN, RMB) és a forintbeli értékét integerként. 

 Hozzunk létre egy Person osztályt is, amiben minden személynél eltárolju, hogy melyik pénznemből hány darab van neki.
 Tároljuk el még a személy korát és nevét is.

 Generáljunk véletlenszerűen embereket különböző típusú és mennyiségű pénzzel. 

 Írjuk ki a konzolra a következőket:
 a.    az emberek vagyonának összértékét
 b.    az emberek vagyonának átlagát
 c.    az emberek vagyonának átlagát 18-49 korosztályban
 d.    az emberek vagyonának medánját
 e.    az emberek nevét vagyonát növekvő sorrendben
 */
package money;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 *
 * @author csaba
 */
public class Money {

    /**
     * @param args the command line arguments
     */
    public static List<Person> persons = new ArrayList<>();

    public static void main(String[] args) {

        generatePersons();

        printPersons();
        //----------------- tesztek-------------------------------------
        System.out.println("Az emberek vagyona összesen: " + calculateFortuneOfAll());
        System.out.println("--------------");
        System.out.println(persons.get(0));
        System.out.println(persons.get(0).getMoneyMap().values());

        System.out.println("vagyon átlag: " + (calculateFortuneOfAll()) / persons.size());
         //----------------- tesztek-------------------------------------

    }

    public static void printPersons() {

        for (Iterator<Person> it = persons.iterator(); it.hasNext();) {
            Person person = it.next();
            System.out.println(person);
        }
    }

    public static void generatePersons() {

        int numOfPersonsToGenerate = randomGenerator(10, 5);
        List<Person> per = new ArrayList<>();
        for (int i = 0; i < numOfPersonsToGenerate; i++) {

            Person person = new Person(generateNameData(), generateAgeData());
            persons.add(person);

            for (Currency cr : Currency.values()) {
                person.setMoneyMap(cr, randomGenerator(100, 1));
            }

            per = persons.stream().filter(person1 -> person1.getAge() > 40).collect(Collectors.toList());

        }
        per.forEach(System.out::println);
        System.out.println("---------------------************************------------------------");
       
    }

    public static int generateAgeData() {

        int age = randomGenerator(50, 18);
        return age;
    }

    public static String generateNameData() {

        String[] firstNames = {"Sándor", "Béla", "Aliz", "Réka", "Géza", "Csilla", "Emese", "László", "Andrea"};
        String[] lastNames = {"Kiss", "Nagy", "Bíró", "Németh", "Radnóti", "Jókai", "Kökény", "Balog", "Nevesincs"};

        String name = null;

        int f = randomGenerator(8, 1);
        int l = randomGenerator(8, 1);

        for (int i = 0; i < lastNames.length; i++) {
            if (i == l) {
                name = lastNames[i];
            }
        }
        for (int j = 0; j < firstNames.length; j++) {
            if (j == f) {
                name += " " + firstNames[j];
            }
        }

        return name;
    }

    public static int randomGenerator(int max, int min) {

        int random = (int) (Math.random() * max + min);
        return random;
    }

    public static int calculateFortuneOfAll() {

        int sum = 0;

        for (Person p : persons) {
            for (Currency c : Currency.values()) {
                sum += c.fortune(c, p.getMoneyMap().get(c));
            }
        }

        return sum;
    }
}
