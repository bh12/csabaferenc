/*
 2. A program töltsön fel véletlenszerűen kétjegyű számokkal egy 500 elemű tömböt, 
 majd csináljon statisztikát róla, melyik szám hányszor fordul elő!

 */
package homework2;

/**
 *
 * @author Csaba Ferenc
 */
public class HomeWork2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        printNumberOccurence(numberOccurence(createRandomArray()));

    }

    public static int[] createRandomArray() {

        int[] array = new int[500];

        for (int i = 0; i < array.length; i++) {

            array[i] = (int) ((Math.random() * 90) + 10);
            System.out.print(array[i] + ", ");
        }
        System.out.println("");
        return array;
    }

    public static int[] numberOccurence(int[] array) {

        int[] statArray = new int[100];

        for (int arrayCounter = 0; arrayCounter < array.length; arrayCounter++) {

            for (int statArrayCounter = 10; statArrayCounter < statArray.length; statArrayCounter++) {

                if (array[arrayCounter] == statArrayCounter) {
                    statArray[statArrayCounter]++;
                }
            }
        }

        return statArray;
    }

    public static void printNumberOccurence(int[] array) {

        System.out.println("Előfordulás: ");

        for (int i = 10; i < array.length; i++) {

            if (array[i] != 0) {

                System.out.println(i + " - szám: " + array[i] + " X fordul elő ");
            }
        }
    }
}
