/*
 5. fibonacci sorozat n. elemének kiszámolására, oldja meg hagyományos és rekurzív módon
 */
package homework5;

/**
 *
 * @author Csaba Ferenc
 */
public class HomeWork5 {

    static int valueOfElement = 0;
    static int lastNumber = 1;
    static int numberBeforeLast = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //teszt
        
        int number = 3;
        System.out.println(calculateElementOfFibonacci(number));
        System.out.println(recoursiveFibonacci(number));

    }

    public static int calculateElementOfFibonacci(int whichElement) {

        int valueOfElement = 0;
        int lastNumber = 1;
        int numberBeforeLast = 0;

        if (whichElement == 1) {
            valueOfElement = 1;
        } else if (whichElement >= 2) {

            for (int i = 1; i < whichElement; i++) {

                valueOfElement = numberBeforeLast + lastNumber;
                numberBeforeLast = lastNumber;
                lastNumber = valueOfElement;
            }
        }
        return valueOfElement;
    }

    public static int recoursiveFibonacci(int whichElement) {

        if (whichElement > 0) {
            valueOfElement = lastNumber + numberBeforeLast;
            lastNumber = numberBeforeLast;
            numberBeforeLast = valueOfElement;
            
            recoursiveFibonacci(whichElement - 1);
        }
        
        return valueOfElement;
    }

}
