/*
 4. A program legyen képes ötös és hatos lottó egy lehetséges húzásának eredményét visszaadni. 
 A program köszöntse a felhasználót, majd egy menüvel döntse el, hogy ötös vagy hatos lottó számokat adjon vissza (menü) 
 */
package homework4;

import java.util.Scanner;

/**
 *
 * @author Csaba Ferenc
 */
public class HomeWork4 {

    public static final int EXIT_MENU_NUMBER = 3;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        welcomeUser();
        menu();

    }

    public static void lotteryFive() {

        int upperBound = 90;

        int[] generatedNumbers = new int[5];

        for (int i = 0; i < generatedNumbers.length; i++) {

            boolean generatedNewNumber;

            do {
                generatedNewNumber = true;

                int generatedNumber = (int) (Math.random() * upperBound) + 1;
                generatedNumbers[i] = generatedNumber;
                for (int j = 0; j < i; j++) {
                    if (generatedNumbers[j] == generatedNumber) {
                        generatedNewNumber = false;
                    }
                }

            } while (generatedNewNumber == false);
        }

        for (int number : generatedNumbers) {
            System.out.print(number + "  ");
        }
        System.out.println("");
    }

    public static void lotterySix() {

        int upperBound = 45;

        int[] generatedNumbers = new int[6];

        for (int i = 0; i < generatedNumbers.length; i++) {

            boolean generatedNewNumber;

            do {
                generatedNewNumber = true;

                int generatedNumber = (int) (Math.random() * upperBound) + 1;
                generatedNumbers[i] = generatedNumber;
                for (int j = 0; j < i; j++) {
                    if (generatedNumbers[j] == generatedNumber) {
                        generatedNewNumber = false;
                    }
                }

            } while (generatedNewNumber == false);
        }

        for (int number : generatedNumbers) {
            System.out.print(number + "  ");
        }
        System.out.println("");
    }

    public static void welcomeUser() {

        System.out.println("Mi a neved? ");

        Scanner sc = new Scanner(System.in);

        String userName = sc.nextLine();

        System.out.println("Üdv, " + userName + "!");
    }

    public static void menu() {

        int userChoice;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Válassz az alábbi menüpontokból:");
            System.out.println("1. ötös lottó számok");
            System.out.println("2. hatos lottó számok ");
            System.out.println(EXIT_MENU_NUMBER + ". kilépés ");

            userChoice = sc.nextInt();
            switch (userChoice) {
                case 1:
                    lotteryFive();
                    break;
                case 2:
                    lotterySix();
                    break;
            }
        } while (userChoice != EXIT_MENU_NUMBER);
    }

}
