/*
3. A program állítson elő véletlenszerűen két megegyező méretű mátrixot! 
Számítsa ki a és írja ki a két mátrix összegét! Minden részfeladatot külön metódus valósítson meg!
 */
package homework3;

/**
 *
 * @author Csaba Ferenc
 */
public class HomeWork3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    public static int randomGenerator (int maxValue){
            
        return ((int) (Math.random() * maxValue + 1));
    }
    
    public static void createArray (){
    
        int lengthOfArray = randomGenerator(10);
        
        int [] [] firstArray = new int [lengthOfArray] [lengthOfArray];
        int [] [] secondArray = new int [lengthOfArray] [lengthOfArray];
        
    }
    
   }
