/*
 1. A program dolgozzon egy 20 elemű tömbbel, töltse fel véletlen kétegyű számokkal! Minden részfeladatot külön metódusba kell megvalósítani
 - Listázza a tömbelemeket
 - Páros tömbelemek összege
 - Van-e öttel osztható szám
 - Melyik az első páratlan szám a tömbben
 - Van-e a tömbben 32

 */
package homework1;

/**
 *
 * @author Csaba Ferenc
 */
public class HomeWork1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

    }

    public static int[] fillArrayWithRandoms() {

        int array[] = new int[20];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) ((Math.random() * 90) + 10);
        }
        return (array);
    }

    public static void printArrayElements(int[] array) {

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.println("");
    }

    public static int sumOfEvenElements(int[] array) {

        int sum = 0;

        System.out.println("Páros tömbelemek összege: ");

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                sum += array[i];
            }
        }
        return (sum);
    }

    public static boolean hasNumberCanBeDevidedByFive(int[] array) {

        for (int i = 0; i < array.length; i++) {

            return (array[i] % 5 == 0);
        }
        return false;
    }

    public static int firstOddNumberInArray(int[] array) {

        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 1) {
                return (i);
            }
        }
        return (-1);
    }

    public static boolean hasThirtyTwooInArray(int[] array) {

        for (int i = 0; i < array.length; i++) {
            return (array[i] == 32);
        }
        return false;
    }

}
