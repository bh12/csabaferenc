
package threadexc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author csaba
 */
public class WriteDataToFile extends Thread { 

    @Override
    public void run() {
        
        File f = new File("TextLines.txt");
        
        try(FileWriter fw = new FileWriter(f);
                BufferedWriter bw = new BufferedWriter(fw)){
        while (!ThreadExc.text.isEmpty()){
         
            bw.write(ThreadExc.text.poll() + System.lineSeparator());
            
        } 
       
        }catch(IOException e){
            System.out.println("File hiba");
        }
        
    }
    
    public WriteDataToFile(){}
}
