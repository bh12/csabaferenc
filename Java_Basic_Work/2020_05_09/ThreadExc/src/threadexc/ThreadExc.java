/*
 Írjon programot, ami egy szálon String sorokat kér be (ez akár lehet a „main” szál is) és
 beteszi az adatot egy Queue-ba, és egy másik szálon pedig ezt a Queue-t felhasználva kiírja
 egy fájlba.
 Ne feledkezzünk a synchronized-ról!

 Addig olvassunk be a konzolról, amíg üres sort nem ütünk be.
 */
package threadexc;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

/**
 *
 * @author csaba
 */
public class ThreadExc {

    public static final Queue<String> text = new PriorityQueue<>();
    private static final Scanner sc = new Scanner(System.in);
    private static String line;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("Szöveg bevitele üres sorig: ");

        do {

            line = sc.nextLine();
            synchronized (text) {
                text.add(line);
            }
        } while (!line.equals(""));

        WriteDataToFile wd = new WriteDataToFile();
        
            wd.start();

    }

}
