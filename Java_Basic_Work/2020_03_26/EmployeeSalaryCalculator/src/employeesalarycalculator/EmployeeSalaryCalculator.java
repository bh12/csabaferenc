/*
 3.    Hozzunk létre egy 2D tömböt a következőnek megfelelően: az első dimenzióban
 tároljuk el az alkalmazottak azonosítóját (egész számok, 1-től növekedve 1-gyel). A második dimenzióban tároljuk el 
 az adott alkalmazotthoz tartozó havi fizetésüket.
 A havi fizetés a következőképp alakul:
 a.    Értékesítő kollégákról van szó, a fizetésük függ attól, hogy mennyi árut adnak el. Az alap bérük 450.000 Ft,
 ehhez adjunk hozzá véletlenszerűen 50.000-125.000 Forintot.
 b.    Minden negyedév végén kapnak véletlenszerűen az adott havi fizetésükhöz képest 5-15 százalékot.
 Írasd  ki a kollégák fizetését hónapra bontva.

 kiegszítés: egész éves adat legyen eltárolva, tehát mind a 12 hónap (véletlenszerűen generált) fizetése. 
 Írjuk ki alkalmazottanként hogy mely hónapokra mekkora fizetést kaptak, és a végén az éves fizetésüket is írjuk ki.

 *********************************************************************************************************************
 .    Az előző feladatot egészítsük ki a következő képpen.
 Adott a következő két String halmaz:
 a.    Laci, Józsi, Béla, Feri, Adri, Zsófi, Károly
 b.    Nagy, Veréb, Molnár, Kiss, Papp, Tóth
 Rendeljünk az előző feladatbeli azonosító számokhoz véletlenszerű neveket a fentebb felsorolt keresztnevek és vezetékneveket felhasználva,
 és így Írasd  ki a kollégák fizetéseit.

********************************************************************************************************************************************
kiegészítés:
+A két String halmazt kapja meg a program parancssorból. Ha nincs megadva adat parancssorból, akkor használjuk az eredeti halmazainkat.
 */
package employeesalarycalculator;


/**
 *
 * @author Csaba Ferenc
 */
public class EmployeeSalaryCalculator {

    public static void main(String[] args) {

        int numberOfEmployees = 10;
        int numberOfMonth = 12;
        int NumberOfDataField = 2;
        int[][][] employeeData = new int[numberOfEmployees][numberOfMonth][NumberOfDataField];

        calculateMonthlySalary(employeeData);

        printSalaryData(employeeData);

    }

    public static void printSalaryData(int[][][] employeeData) {

        for (int i = 0; i < employeeData.length; i++) {

            int salaryForYear = 0;
            String employeeName = randomEmployeeNames();

            for (int j = 0; j < employeeData[i].length; j++) {
                
                salaryForYear += employeeData[i][j][1];
                System.out.print("Hónap: \t" + namesOfMonthes(j));
                System.out.print("\t Azonosító: " + employeeData[i][j][0] + "\t" + employeeName + "\t");
                System.out.print("Fizetés: " + employeeData[i][j][1] + System.lineSeparator());
            }
            System.out.println("--------------------------------------------------------");
            System.out.println("Éves fizetés: " + salaryForYear);
            System.out.println("********************************************************");
        }
    }

    public static void calculateMonthlySalary(int[][][] employeeData) {

        double quarterYearBonus = randomFiveToFifteen() / 100.0 + 1;

        for (int i = 0; i < employeeData.length; i++) {

            for (int j = 0; j < employeeData[i].length; j++) {
                employeeData[i][j][0] = i + 1;
                if ((j + 1) % 3 == 0) {
                    employeeData[i][j][1] = (int) ((450_000 + calculateMonthlyBonus()) * quarterYearBonus);
                } else {
                    employeeData[i][j][1] = 450_000 + calculateMonthlyBonus();
                }
            }
        }
    }

    public static int calculateMonthlyBonus() {
        return (int) (Math.random() * 75000 + 50000);
    }

    public static int randomFiveToFifteen() {
        return (int) (Math.random() * 10 + 5);

    }

    public static String namesOfMonthes(int numberOfMonth) {

        String[] monthes = {"Január", "Február", "Március", "Április",
            "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"};

        int elementMaxLength = 0;

        for (int i = 0; i < monthes.length; i++) {

            if (monthes[i].length() > elementMaxLength) {
                elementMaxLength = monthes[i].length();
            }
        }

        for (int i = 0; i < monthes.length; i++) {

            do {

                if (monthes[i].length() < elementMaxLength) {
                    monthes[i] += " ";
                }
            } while (monthes[i].length() != elementMaxLength);
        }

        return monthes[numberOfMonth];
    }

    public static String randomEmployeeNames() { //String [] args

        String firstName = "Laci,Józsi,Béla,Feri,Adri,Zsófi,Károly";
        String lastName = "Nagy,Veréb,Molnár,Kiss,Papp,Tóth";

        String[] firstNameArray = firstName.split(",");
        String[] lastNameArray = lastName.split(",");

        int firstRandom = (int) (Math.random() * firstNameArray.length);
        int secondRandom = (int) (Math.random() * lastNameArray.length);

        return (firstNameArray[firstRandom] + " " + lastNameArray[secondRandom]);
    }

}
