package classesexc;

/**
 *
 * @author csaba
 */
import java.util.Scanner;

public class SmartCalculator {

    public static Scanner sc = new Scanner(System.in);

    private int number;
    private int counter = 0;

    private char operation;//+,-
    private double result;

    //nem kell konstruktor
    public void askNumber() {
        System.out.println("Kérem addjon meg egy számot: ");
        this.number = sc.nextInt();
        if (counter == 0) {
            result = this.number;
            counter++;
        }
        askOperation();
    }

    public void askOperation() {
        System.out.println("Kérem addjon meg egy műveletet (+,-,=): ");
        char op = sc.next().charAt(0);

        switch (op) {
            case '=':
                printResult();
                break;
            default:
                this.operation = op;
                handleEqualOperation();
                askNumber();
                break;
        }
    }

    private void handleEqualOperation() {
        switch (this.operation) {
            case '+':
                result += number;
                break;
            case '-':
                result -= number;
                break;
        }
    }

    public void printResult() {
        System.out.println("result: " + this.result);
    }

    public void start() {
        askNumber();
    }
}
