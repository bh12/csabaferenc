/*
 HF:
Csináljatok egy órát megvalósító osztályt, az órának három adattagja legyen:
- óra
- perc
- másodperc
Tudja kiírni hogy mennyi az idő (toString), lehessen hozzáadni még perceket (plusMinute(int minute) metódus), ami változtatja a percek számát.
 */
package classesexc;

/**
 *
 * @author csaba
 */
public class Clock {
    
    private int hours;
    private int minutes;
    private int seconds;
    final int HOUR_IN_MINUTES = 60;
    
    public Clock(int hours, int minutes, int seconds){
    this.hours = hours;
    this.minutes = minutes;
    this.seconds = seconds;
    }
    
    public void printTime (){
        System.out.println(hours + " : " + minutes + " : " + seconds);
    }
     
    public void plusMinute (int plusMinutes){
     
        int totalMinutes = minutes + plusMinutes;
        
        if (totalMinutes < HOUR_IN_MINUTES){
        minutes += plusMinutes;
        } 
        else {
        hours += totalMinutes / HOUR_IN_MINUTES;
        minutes = totalMinutes % HOUR_IN_MINUTES;
        }
        
    }
}
