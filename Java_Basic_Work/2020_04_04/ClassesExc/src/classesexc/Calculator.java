/*
 rendelkezzen 2 szám adattaggal és legyen négy metódusa (+,-,*,/)
 */
package classesexc;

/**
 *
 * @author csaba
 */
public class Calculator {
    private int a;
    private int b;
    
    public Calculator (int a, int b){
    this.a = a;
    this.b = b;
    }
    
    public int getA(){
    return a;
    }
    
    public int getB(){
    return b;
    }
    public void setA(int a){
    this.a = a;
    }
    
    public void setB (int b){
    this.b = b;
    }
    
    public int addition(){
    return a + b;
    }
    
    public int substraction(){
    return a - b;
    }
    
    public int multiplication(){
    return a*b;
    }
    
    public double devision(){
    return (double)a/b;
    }
}
