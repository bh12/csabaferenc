package lotterywitharrays;

import java.util.Scanner;

/**
 *
 * @author Csaba Ferenc
 */
public class LotteryWithArrays {

    public static void main(String[] args) {

        userLotteryNumbers();
        generateLotteryNumber();
        matchChecker();
        
    }

    public static int[] generateLotteryNumber() {

        int upperBound = 90;

        int[] generatedNumbers = new int[5];

        for (int i = 0; i < generatedNumbers.length; i++) {

            boolean generatedNewNumber;

            do {
                generatedNewNumber = true;

                int generatedNumber = (int) (Math.random() * upperBound) + 1;
                generatedNumbers[i] = generatedNumber;
                for (int j = 0; j < i; j++) {
                    if (generatedNumbers[j] == generatedNumber) {
                        generatedNewNumber = false;
                    }
                }

            } while (generatedNewNumber == false);
        }

        System.out.println("Kisorsolt számok: ");

        for (int number : generatedNumbers) {
            System.out.print(number + "  ");
        }
        System.out.println("");
        return generatedNumbers;
    }

    public static int[] userLotteryNumbers() {

        System.out.println("Kérem a tippeket: ");

        Scanner sc = new Scanner(System.in);

        int upperBound = 90;

        int[] userNumbers = new int[5];

        for (int i = 0; i < userNumbers.length; i++) {

            boolean userNewNumber;

            do {
                userNewNumber = true;

                int userNumber = sc.nextInt();
                userNumbers[i] = userNumber;
                for (int j = 0; j < i; j++) {
                    if (userNumbers[j] == userNumber) {
                        userNewNumber = false;
                    }
                }

            } while (userNewNumber == false);
        }

        System.out.println("Megjátszott számok: ");

        for (int number : userNumbers) {
            System.out.print(number + "  ");
        }
        System.out.println("");
        return userNumbers;
    }

    public static int matchChecker (){
    
        int matchCounter = 0;
        int [] generatedNumbers = generateLotteryNumber();
        int [] userNumbers = userLotteryNumbers();
       
        for (int i = 0; i < generatedNumbers.length; i++){
        
        for (int j = 0; j < userNumbers.length; j++){
        
            if (generatedNumbers[i] == userNumbers[j]){
            matchCounter++;
            }
        }
        }
        System.out.println("Találatok száma: " + matchCounter);
        return matchCounter;
    }
    
}
