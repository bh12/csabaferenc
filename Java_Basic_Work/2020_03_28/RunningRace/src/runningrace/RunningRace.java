/*
 1. futóverseny eredményeinek feldolgozása: kérjük be a felhasználótól hány futó volt 
 (legalább 3 ember kell a versenyhez), majd kérjük be a futók nevét és hogy hány perc alatt teljesítették a távot,
 majd a végén írjuk ki az első három helyezett futót és eredményeit.

 */
package runningrace;

import java.util.Scanner;

/**
 *
 * @author Csaba Ferenc
 */
public class RunningRace {

    public static final int numberOfPlaces = 3;
    public static int numberOfRunners;
    public static String[] namesOfRunners = new String[numberOfRunners];
    public static int[] timeOfRunnings = new int[numberOfRunners];

    public static void main(String[] args) {

        runnersDataInput();

        checkingPlaces();

        printPlaces(namesOfRunners, timeOfRunnings);
    }

    public static void runnersDataInput() {
        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Kérem adja meg a futók számát: ");
            numberOfRunners = sc.nextInt();
        } while (numberOfRunners < 3);
        
        for (int counter = 0; counter < numberOfRunners; counter++) {
            System.out.println(counter + 1 + ". futó neve: ");
            namesOfRunners[counter] = sc.next();
            System.out.println(counter + 1 + ". futó ideje: ");
            timeOfRunnings[counter] = sc.nextInt();
        }
    }

    public static void checkingPlaces() {
        for (int i = 0; i < timeOfRunnings.length; i++) {
            for (int j = timeOfRunnings.length - 1; j > i; j--) {
                if (timeOfRunnings[i] > timeOfRunnings[j]) {
                    int tmp = timeOfRunnings[i];
                    String namesTmp = namesOfRunners[i];
                    namesOfRunners[i] = namesOfRunners[j];
                    namesOfRunners[j] = namesTmp;
                    timeOfRunnings[i] = timeOfRunnings[j];
                    timeOfRunnings[j] = tmp;
                }
            }
        }
    }

    public static void printPlaces(String[] namesOfRunners, int[] timeOfRunners) {
        for (int counter = 0; counter < numberOfPlaces; counter++) {
            System.out.println(counter + 1 + ". helyezett: " + namesOfRunners[counter]);
            System.out.println(timeOfRunners[counter]);
        }
    }

}
